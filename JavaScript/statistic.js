var generateStatsBtn = document.getElementById('generateStats');
var videoStatTableTitle = document.getElementById('videoStatTableTitle');
var userStatTableTitle = document.getElementById('userStatTableTitle');
var commentStatTableTitle = document.getElementById('commentStatTableTitle');
var viewEntriesStatTableTitle = document.getElementById('viewEntriesStatTableTitle');
var videoTitleArr = [];
var videoCommentArr = [];
var userDataArr =[];
var viewEntriesArr = [];


$(document).ready(function(){
    
    database.ref('/Comments').on('value', function(snapshot) {
        videoCommentArr.push(snapshotToArray(snapshot));
      });
  
      database.ref('/Users').on('value', function(snapshot) {
        userDataArr.push(snapshotToArray(snapshot));
      });
    
      database.ref('/Videos').on('value', function(snapshot) {
        videoTitleArr.push(snapshotToArray(snapshot));
      });
   
      database.ref('/viewEntries').on('value', function(snapshot) {
        if (snapshot.exists()){
          viewEntriesArr.push(snapshotToArray(snapshot));
        }
      });

    emptyTable();
});


function emptyTable(){
    $('#videoStatTable').empty();
    $('#userStatTable').empty();
    $('#commentStatTable').empty();
    $('#viewEntriesStatTable').empty();
}



// Converting database snapshot into array format
function snapshotToArray(snapshot) {
    var returnArr = [];
  
    snapshot.forEach(function(childSnapshot) {
        var item = childSnapshot.val();
        item.key = childSnapshot.key;
        
        returnArr.push(item);
    });
  
    return returnArr;
  };


  
// Clicking Generate Report Button to display all tables with data
generateStatsBtn.onclick = function(){

    emptyTable();

    videoStatTableTitle.style.visibility ='visible';
    userStatTableTitle.style.visibility ='visible';
    commentStatTableTitle.style.visibility ='visible';
    viewEntriesStatTableTitle.style.visibility ='visible';

    //Dinamically generating VIDEO table with data
    var videoInfoTableString ='<thead class="thead-dark">'+
    '<tr>'+
      '<th scope="col">#</th>'+
      '<th scope="col">VideoID</th>'+
      '<th scope="col">Title</th>'+
      '<th scope="col">Length</th>'+
      '<th scope="col">Views</th>'+
      '<th scope="col">Likes</th>'+
    '</tr>'+
    '</thead>'+
    '<tbody>';
     for(var i=0; i< videoTitleArr[0].length; i++){
      videoInfoTableString += 
      '<tr>'+
        '<th scope="row">'+i+'</th>'+
        '<td>'+videoTitleArr[0][i]['key']+'</td>'+
        '<td>'+videoTitleArr[0][i]['Title']+'</td>'+
        '<td>'+videoTitleArr[0][i]['VideoLength']+'</td>'+
        '<td>'+videoTitleArr[0][i]['Views']+'</td>'+
        '<td>'+videoTitleArr[0][i]['Likes']+'</td>'+
      '</tr>';
    } 
    videoInfoTableString += '</tbody>';


    //Dinamically generating USER table with data
    var userInfoTableString = '<thead class="thead-dark">'+
    '<tr>'+
      '<th scope="col">#</th>'+
      '<th scope="col">Photo</th>'+
      '<th scope="col">UserName</th>'+
    '</tr>'+
    '</thead><tbody>';
    for(var i=0; i< userDataArr[0].length; i++){
        userInfoTableString += 
        '<tr>'+
          '<th scope="row">'+i+'</th>'+
          '<td><img src="'+userDataArr[0][i]['ImageURL']+'" class="img-thumbnail avatar"></td>'+
          '<td>'+userDataArr[0][i]['key']+'</td>'+
        '</tr>';
    } 
    userInfoTableString +='</tbody>';


    //Dinamically generating COMMENTS table with data
    var commentsTableString ='<thead class="thead-dark">'+
    '<tr>'+
      '<th scope="col">#</th>'+
      '<th scope="col">VideoID</th>'+
      '<th scope="col">UserName</th>'+
      '<th scope="col">Comments</th>'+
      '<th scope="col">Date/Time</th>'+
    '</tr>'+
  '</thead><tbody>';

    var counter = 0;
    videoCommentArr[0].forEach(commentsArray => {

        var videoID = commentsArray['key'];
        var eachPersonName = Object.keys(commentsArray);
        var eachPersonComments = Object.values(commentsArray);
    
            for(var i=0; i< eachPersonName.length -1; i++){
                  var commentText = Object.values(eachPersonComments[i]);
                  var dates = Object.keys(eachPersonComments[i]);

                    for(var k=0; k< commentText.length; k++){
                        counter +=1;
                        commentsTableString += 
                            '<tr>'+
                                '<th scope="row">'+counter+'</th>'+
                                '<td>'+videoID+'</td>'+
                                '<td>'+eachPersonName[i]+'</td>'+
                                '<td>'+commentText[k]+'</td>'+
                                '<td>'+dates[k]+'</td>'+
                            '</tr>';   
                    } 
            }
    }); 
    commentsTableString +='</tbody>';

   //Dinamically generating VIEW ENTRY table with data
   var viewEntriesTableString='';
   if(viewEntriesArr.length==0){
        viewEntriesTableString +='<div>No data yet!</div>';
   }else{
        viewEntriesTableString += '<thead class="thead-dark">'+
        '<tr>'+
          '<th scope="col">#</th>'+
          '<th scope="col">EntryID</th>'+
          '<th scope="col">VideoID</th>'+
          '<th scope="col">Video Length</th>'+
          '<th scope="col">Video End Time</th>'+
          '<th scope="col">Complete</th>'+
          '<th scope="col">Like</th>'+
          '<th scope="col">UserName</th>'+
          '<th scope="col">Video Date/Time</th>'+
        '</tr>'+
      '</thead><tbody>';

        for(var i=0; i<viewEntriesArr[0].length;i++){
                viewEntriesTableString += 
                            '<tr>'+
                                '<th scope="row">'+(i+1)+'</th>'+
                                '<td>'+viewEntriesArr[0][i]['key']+'</td>'+
                                '<td>'+viewEntriesArr[0][i]["VideoId"]+'</td>'+
                                '<td>'+(Math.round(viewEntriesArr[0][i]["VideoLength"] * 100) / 100)+'</td>'+
                                '<td>'+(Math.round(viewEntriesArr[0][i]["VideoEndTime"] * 100) / 100)+'</td>'+
                                '<td>'+viewEntriesArr[0][i]["Complete"]+'</td>'+
                                '<td>'+viewEntriesArr[0][i]["Like"]+'</td>'+
                                '<td>'+viewEntriesArr[0][i]["User"]+'</td>'+
                                '<td>'+viewEntriesArr[0][i]["ViewDateTime"]+'</td></tr>';

        }
        viewEntriesTableString += '</tbody>';
   }//end of if-else
 
   

  $('#videoStatTable').prepend(videoInfoTableString); 
  $('#userStatTable').prepend(userInfoTableString); 
  $('#commentStatTable').prepend(commentsTableString); 
  $('#viewEntriesStatTable').prepend(viewEntriesTableString);  

  }