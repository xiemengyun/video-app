var video = document.querySelector('.video');
var juice = document.querySelector('.orange-juice');
var playBtn = document.getElementById('play-pause');
var previousBtn = document.getElementById('previous');
var nextBtn = document.getElementById('next');
var commentInput = document.getElementById('comments');
var commentBtn = document.getElementById('submitComment');
var progressBar = document.getElementById('progressBar');
var videoTitle = document.getElementById('videoTitle');
var likeBtn = document.getElementById('likeBtn');
var likeCountlabel = document.getElementById('likeCount');


var hasViewed = false;
var hasLiked = 'Yes';
var likeCounter = 0;
var commentRows ='';


// Loading/Preparing initial dummy data 
$(document).ready(function() {
  dataLoading();
  loadComments();
});


// clicking the play/pause button of the video
function togglePlayPause(){
  if(video.paused){
    playBtn.className = 'pause';
    video.play();
  }else{
    playBtn.className ='play';
    video.pause();
  }
}


$('#comments').keyup(function(){
  if($(this).val().length !=0)
    $('#submitComment').attr('disabled', false);            
  else
    $('#submitComment').attr('disabled', true);        
})

// clicking the video play button
playBtn.onclick = function(){
  hasViewed = true;
  togglePlayPause();
  
}

// Dynamically moving the orange progress bar
video.addEventListener('timeupdate',function(){
  var juicePosition = video.currentTime / video.duration;
  juice.style.width = juicePosition * 100 +"%";
  if(video.ended){
    btnClassName ="play";
  }
})


// Clicking the video progress bar to forward/backward the video
progressBar.addEventListener('click', function(e) {
  var percentage = (e.pageX - 255) / (1024 - 255);
  video.currentTime = percentage * video.duration;
}); 

// Set the video content when click previous/next or whenever the video gets loaded
function setVideoContent(videoIndex, videoTitleStr){
  var source = '../Videos/Video'+videoIndex+'.mp4';
  videoTitle.textContent = videoTitleStr;
  juice.style.width = 0.1 +"%";
  video.setAttribute('index',videoIndex);
  video.setAttribute('src',source);
  
}


// Clicking the "Previous" (<) button
 previousBtn.onclick = function(){
   var currentVideoIndex = video.getAttribute('index');
   var previousIndex = 0;
   var videoTitleStr ='';

   if(currentVideoIndex == 1){
        previousIndex = 5;
        videoTitleStr = videoTitleArr[0][4]['Title'];;
   }else{
        previousIndex = parseInt(currentVideoIndex) - 1;
        videoTitleStr = videoTitleArr[0][currentVideoIndex-2]['Title'];;
   }

   if(hasViewed == true){
    insertViewEntries();
   }

   setVideoContent(previousIndex,videoTitleStr);
   playBtn.className ='play';
   commentInput.value ='';
   likeCounter = 0;
   likeBtn.setAttribute('class','btn btn-light');
   clearCommentTable()
   loadComments();
   updateLikes();
   hasViewed = false;
   
   
} 

// Clicking the "Next" (>) button
nextBtn.onclick = function(){
  var currentVideoIndex = video.getAttribute('index');
  var nextIndex = 0;
  var videoTitleStr ='';

   if(currentVideoIndex == 5){
        nextIndex = 1;
        videoTitleStr = videoTitleArr[0][0]['Title'];
   }else{
        nextIndex = parseInt(currentVideoIndex)  + 1;
        videoTitleStr = videoTitleArr[0][currentVideoIndex]['Title'];
   }

   if(hasViewed == true){
    insertViewEntries();
   }

  setVideoContent(nextIndex, videoTitleStr);
  playBtn.className ='play';
  commentInput.value ='';
  likeCounter = 0;
  likeBtn.setAttribute('class','btn btn-light');
  clearCommentTable()
  updateLikes();
  loadComments();
  hasViewed = false;

}


// Clicking the like button
likeBtn.onclick = function(){
  likeCounter +=1;
  var likeCountText = parseInt(likeCountlabel.textContent);

  if(likeCounter % 2 == 0){
    likeBtn.setAttribute('class','btn btn-light');
    likeCountlabel.textContent = likeCountText-1;
    hasLiked = 'No';
  }
  else{
    likeBtn.setAttribute('class','btn btn-danger');
    likeCountlabel.textContent = likeCountText+1;
    hasLiked = 'Yes';
  }
  
}


// Retrieving the system date and time 
function getSystemDateTime(){
  var systemDate = new Date();
  var dd = systemDate.getDate();
  var mm = systemDate.getMonth()+1; 
  var yyyy = systemDate.getFullYear();
  var hour = systemDate.getHours();
  var min = systemDate.getMinutes();
  var sec = systemDate.getSeconds();
  var todayDate = yyyy + '-' + mm + '-' + dd + ' ' + hour + ':' + min + ':' + sec;

  return todayDate;
}

// Clicking the comment submit button
commentBtn.onclick = function(){
  var todayDate = getSystemDateTime();

  var commentRow = 
  '<tr>'+
    '<td>'+
        '<div id="avatorDiv"><img src="../images/noname.png" alt="..." class="img-thumbnail avatar"></div>'+
        '<div id="infoDiv"><div>Me</div> <div>'+todayDate+'</div></div>'+
    '</td>'+
    '<td class="bold">'+commentInput.value+'</td>'+
  '</tr>';

  $('#commentContent').prepend(commentRow);
  insertComment(todayDate,commentInput.value);
  commentInput.value ='';
}

// Inserting comments into database
function insertComment(todayDate,commentText){

  var currentVideoIndex = video.getAttribute('index');
  var currentVideoId = "Video"+currentVideoIndex;

  database.ref('Comments/'+currentVideoId+'/Me').update({
    [todayDate]:commentText
});

}

// Loading the comments when the page is refreshed or loaded 
function loadComments(){

  var currentVideoIndex = video.getAttribute('index');
  var currentVideoId = "Video"+currentVideoIndex;
  var commentForThisVideo = [];
  likeCountlabel.textContent = videoTitleArr[0][currentVideoIndex-1]['Views'];

  database.ref('/Comments/'+currentVideoId).on('value', function(snapshot) {
    commentForThisVideo.push(snapshotToArray(snapshot));
  });

  commentForThisVideo[0].forEach(commentsArray => {
    
    var user = commentsArray['key'];
    var dates = [];
    var commentText = [];
    var imageURL = '';

    dates = Object.keys(commentsArray);
    commentText = Object.values(commentsArray);
    
    database.ref('/Users/'+user).on('value', function(snapshot) {
      imageURL = snapshot.child('ImageURL').val();
    });
    
     for(var i=0; i< commentText.length -1; i++){
      commentRows += 
      '<tr>'+
        '<td>'+
            '<div id="avatorDiv"><img src="'+imageURL+'" class="img-thumbnail avatar"></div>'+
            '<div id="infoDiv"><div class="boldText">'+user+'</div> <div>'+dates[i]+'</div></div>'+
        '</td>'+
        '<td>'+commentText[i]+'</td>'+
      '</tr>';

    } 
  }); 

  $('#commentContent').prepend(commentRows);
  commentRows ='';
  
}// End of loadComment function


// Update Video table likes number
function updateLikes(){
  var currentVideoIndex = parseInt(video.getAttribute('index'))-1;
  var currentVideoId = "Video"+currentVideoIndex;
  database.ref('Videos/'+(currentVideoId)+'/Views').set(likeCountlabel.textContent);
}


// Inserting viewEntries whenever user has watched the video
function insertViewEntries(){
  var entryID = "entry"+getSystemDateTime();
  var videoLength =  video.duration;
  var currentVideoIndex = video.getAttribute('index');
  var currentVideoId = "Video"+currentVideoIndex;
  var videoEndTime = video.currentTime;
  var complete = ""; 

  if(parseInt(videoEndTime)==parseInt(videoLength)){
    complete = "Yes";
  }else{
    complete = "No";
  }

  database.ref("viewEntries").child(entryID).set({
    "Like": hasLiked,
    "Complete": complete,
    "VideoEndTime": videoEndTime,
    "VideoId": currentVideoId,
    "User": "Me",
    "ViewDateTime": getSystemDateTime(),
    "VideoLength": videoLength
  });

}

function clearCommentTable(){
  $('#commentContent').empty();
}


