// Initialize Firebase
var config = {
  apiKey: "AIzaSyBpeDmiScpMuvJwxDw9sudiOGu5BZSo-fE",
  authDomain: "vuejs-video-app.firebaseapp.com",
  databaseURL: "https://vuejs-video-app.firebaseio.com",
  projectId: "vuejs-video-app",
  storageBucket: "vuejs-video-app.appspot.com",
  messagingSenderId: "369333894158"
};
firebase.initializeApp(config);

var database = firebase.database();
var videoTitleArr = [];
var videoCommentArr = [];
var userDataArr =[];
var viewEntriesArr = [];


function dataLoading(){
    database.ref().remove();

    // create dummy videos data
    database.ref('Videos').child('Video1').set({ 
      "Title" : 'NG test Video Setup guide',
      "Likes" : 12, 
      "Views" : 333,
      "VideoLength" : 332.5
    });
  
    database.ref('Videos').child('Video2').set({ 
      "Title" : 'Canon EOS 700D DSLR Intro',
      "Likes" : 4, 
      "Views" : 123,
      "VideoLength" : 254.4
    });
  
    database.ref('Videos').child('Video3').set({ 
      "Title" : 'Adidas Yeezy Boost 350 Commercial',
      "Likes" : 67, 
      "Views" : 80,
      "VideoLength" : 47
    });
  
    database.ref('Videos').child('Video4').set({ 
      "Title" : 'Nike VaporMax 2018 Commercial',
      "Likes" : 43, 
      "Views" : 29,
      "VideoLength" : 84.4
    });
  
    database.ref('Videos').child('Video5').set({ 
      "Title" : 'John lewis & Partners',
      "Likes" : 88, 
      "Views" : 872,
      "VideoLength" : 140
    });
  
    
    // create dummy users data
    database.ref('Users').child('Allen').set({"ImageURL": "../images/Allen.png"});
    database.ref('Users').child('Lily').set({"ImageURL": "../images/Lily.png"});
    database.ref('Users').child('Marvin').set({"ImageURL": "../images/Marvin.png"});
    database.ref('Users').child('Jornny').set({"ImageURL": "../images/Jornny.png"});
    database.ref('Users').child('Susan').set({"ImageURL": "../images/Susan.png"});
    database.ref('Users').child('Me').set({"ImageURL": "../images/noname.png"});
  
    // create dummy comments data
    database.ref('Comments').child('Video1').set({
      "Allen": {
        "2017-4-23 22:00:45":"Nice Video! Thanks for Sharing!",
        "2017-4-24 21:23:00":"Btw i wanna get one as well"
      },
      "Lily":{
        "2017-4-27 13:00:33":"Well done!!",
      },
      "Susan":{
        "2017-5-12 02:22:20":"Please post more videos like this!",
      },
      "Jornny":{
        "2017-7-01 03:13:43":"Cool dude!",
      }
    });
  
    database.ref('Comments').child('Video2').set({
      "Marvin": {
        "2018-1-01 18:37:28":"I want this camera as well!",
      },
      "Jornny":{
        "2018-2-19 19:55:54":"Is this camera the lastest one?",
      },
      "Susan":{
        "2018-3-12 22:00:00":"Please write more review about this DSLR, thx!",
      },
      "Lily":{
        "2018-9-01 20:17:45":"looking forward to your next video!",
      }
    });
  
    database.ref('Comments').child('Video3').set({
      "Jornny": {
        "2018-3-03 16:44:45":"Finally! Yezzy Boost 350 is here!",
      },
      "Susan":{
        "2018-2-20 12:10:06":"Nice kicks! Drop or Cop?",
        "2018-2-19 19:22:08":"Btw I was wondering how much it costs? Hopefully it wont cost much.",
      },
      "Marvin":{
        "2018-3-02 18:30:14":"Gonna get one! Definitely!!!",
      }
    });
  
    database.ref('Comments').child('Video4').set({
      "Marvin":{
        "2018-5-28 14:55:30":"Very well made commercial! Nike rocks!",
      }
    });
  
    database.ref('Comments').child('Video5').set({
      "Lily":{
            "2018-7-01 15:20:29":"All time my favourite super star! But he is getting old!",
      }
    });
  
    pushingDatatoArray();
}

function pushingDatatoArray(){
    // Pushing the data into arrays
    database.ref('/Comments').on('value', function(snapshot) {
      videoCommentArr.push(snapshotToArray(snapshot));
    });

    database.ref('/Users').on('value', function(snapshot) {
      userDataArr.push(snapshotToArray(snapshot));
    });
  
    database.ref('/Videos').on('value', function(snapshot) {
      videoTitleArr.push(snapshotToArray(snapshot));
    });
 
    database.ref('/viewEntries').on('value', function(snapshot) {
      if (snapshot.exists()){
        viewEntriesArr.push(snapshotToArray(snapshot));
      }
    });
}


// Converting database snapshot into array format
function snapshotToArray(snapshot) {
    var returnArr = [];
  
    snapshot.forEach(function(childSnapshot) {
        var item = childSnapshot.val();
        item.key = childSnapshot.key;
  
        returnArr.push(item);
    });
  
    return returnArr;
  };